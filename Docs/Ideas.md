**OG Tatt** is a top-down urban shoot-em-up set in a world called Art where respect is only the beginning. 
Players can play without joining an existing gang. Play on your own and create a gang with at least three people whenever you feel like it. You'll have to earn enough respect to get an HQ and to be mentioned.  
HQs 
 
# Gameplay

If you decide to join a gang you can either fight other gangs in each city or smuggle warez to other HQs of your gang in other cities.

## Controls

* Keyboard
   - `Enter`: Enter/Leave nearest vehicle on the most convenient side
   - `]`: Specifically enter/leave nearest vehicle on the right side
   - `[`: Specifically enter/leave nearest vehicle on the left side
 
## UI

+ Ability to create animated avatar from _building blocks_
+ Chat as pop-up with avatar and message for people on-screen and.
 
## Hand-to-Hand combat

+ Disarm
+ Counter
+ Grab/Chokehold (PvP: buttonbash to break neck/free)

-----------------------
   
## World

A vehicle is required to get to another city by road. Traveling to another city the camera angle changes and it is like a highway racing game during which you'll see the environment change in accordance with the climate and terrain and cultural region.

### Money `[ ϕ ]`

The world of Art uses a single currency, the ram, denoted with the symbol **ϕ**.
Some values have their own names:

Value | Name
---|---
ϕ1,- | Buck
ϕ10, | Croc
ϕ20, | Canary
ϕ50, | Flamingo


### Overview

![Home](Globe.png)

![Map](WorldMap.png)

### Regions

#### Western

* **Grunstatt** (Dutch/German/Danish) | Hemp
* **Ånslo** (Norwegian/Swedish/Finnish) | Fishballs
* **Noopolis** (Italian/Greek/French) | Cheese, wine and philosophy
* **Airstrip 1** (British/American) | Telescreens

#### Southern

* **Zombuntu** (Savannah) | Wildlife and witchdoctors
* **Agrobar** (Desert) - Kekistan | Oil
* **Axllotus** (Jungle) | Natives
* **Ichell** (Steppe) | 

#### Eastern

* **Pingrad** (Russian/North Korean) | Communism
* **Balkan** (Eastern European) | 
* **Monkong** (Chinese/Tibetan/Japanese) | Pandas, robots and monks
* **Sloompur** (Australian/Indonesian/New Zealand) | Giant squids, small islands

### Vehicles

* Land
   - Cars
   - Motorcycles
   - Tanks
   - Boards

* Water
   - Rowboats
   - Speedboats
   - Jet-skis

### Weapons

##### Light weapons

+ Pistol: _Little Elvis_
+ Laser pistol: _Pincher_
+ SMG: _Spitfire_
+ Gyrojet: _Fracker_
+ Molotov cocktail


##### Heavy weapons

Slows you down somewhat, only one allowed.

+ Mini-gun: _Herzstark_
+ Flamethrower: _Libertorch_
+ Rocket Launcher: _Deathfist_
+ Explosive Drones: _Freedom Delivery System_
+ Electric arc: _Shocker_ (chains, revives fish and powers/disturbs electric lights)
 
### Gangs

For a list of gangs see [Gangs.md](Gangs.md)

Each will include:

+ Emblem and colors (applied to characters banners, vehicles)
+ Voices for both sexes
+ Accessories for character customization
+ Building(s)

### Shops

- **Bonbon Ted** - chocolatier
- **Fresh Meat** - butcher
- **Them Apples** - grocery store
- **Big Cheeze** - cheese store
- **Hot Buns** - bakery

#### Mobile

- **Fish van** (Western) "Fishheads" honk
- **Lumpia trike** (Eastern) "bell" honk
- **Couche couche train** (Southern) "Couche" honk

### Brands and commercials
Völva - "Bil di papa"

### Inspiration

- GTA2
- Postal
- The Matrix
- Illuminatus Trilogy