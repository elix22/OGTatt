
<style>
body {
background-color: #cccccc;
}

.colorBar {
  width: 100%;
  height: 23px;
}

img{

width: 96px;
padding: 23px 42px;
}
</style>

-----

#### Tourists

<img src="../Resources/Textures/Decals/Tourists.png" style="background-color:white; border: 23px solid lightgrey;"" />

Just enjoying their holiday in a country they love... no really!  
After creating a new character this is your default alignment. During character creation emblems of the gangs show how the appearance you pick effects your ability to join each of them. When leaving a gang you become a tourist again and the respect you gained is turned into a wanted level for that gang.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
None | Camera, Shades, Shorts, Hawaii blouse, Straw hat |  | Midtone skin | None | None

-----------------------

#### Chaosquad
 
<img src="../Resources/Textures/Decals/Chaosquad.png" style="background-color:grey; border: 23px solid indigo;"/>

Everything is magic if you look at it that way.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Anywhere | Witches hat, Robe | Cannot be bothered when someone leaves | None | None | None

-----------------------

#### Association of Autonomous Astronauts

<img src="../Resources/Textures/Decals/AAA.png" style="background-color: silver; border: 23px solid gold;" />

Trying to get away from this place these nerds ran into a little budget problem. Nevertheless they have gained some experience with rockets along the way. Often they need parts that only Zebratools provides at frustratingly high prices.

##### Upkeep: ϕ 100,-


Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Pingrad (HQ), Balkan, Grunstatt and Airstrip 1 | Space helmet, Backpack | Jet pack and nitro | Midtone skin | Rat | ϕ 100,-

-----------------------

#### Obscurinati

<img src="../Resources/Textures/Decals/Obscurinati.png" style="background-color:fuchsia; border: 23px solid black;"/>

Having spent too much time in the dark these suits were cast back down to earth from their space thrones to fight for their life like any other mere mortal. Their archenemies are the Luciferians as Art is too small for _two_ forces of darkness.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Zombuntu (HQ, triangular "pentagon"), Airstrip 1, Agrobar, Ichell | Baseball cap | Stealth (pink = transparency), Jamming |  | Ox | ϕ 555,-

-----------------------

#### Pantherae Byssii

<img src="../Resources/Textures/Decals/BeigePanthers.png" style="background-color: brown; border: 23px solid pink;" />

The bastard child of white supremacy and the black panther movement. These "mixed-race" individuals have had it with not being acknowledged as a race of their own resulting in the extremist _neo genus_ doctrine. Shortly after their inception the rest of the world realized the absurdity of racism.


Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Agrobar (HQ), Zombunto, Noopolis, Balkan |  |  | Midtone skin | Tiger | ϕ 88,-

-----------------------

#### Confusians

<img src="../Resources/Textures/Decals/Confusians.png" style="background-color:orange; border: 23px solid grey;"/>

Hail Eris! Whatever that means. Their disorganized approach helped these Discordians in claiming the position of biggest religion. The SubGenii constantly trying to come up with news ways to change this.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Monkong (HQ), Grunstatt,  Pingrad, Sloompur |  | Apple (single health boost) |  | Rabbit | ϕ 23,-

-----------------------

#### Project Rainbow

<img src="../Resources/Textures/Decals/Hippies.png" style="background: linear-gradient(to right, orange, yellow, green, cyan, blue, fuchsia, orange); background-size: 50% 100%; border: 23px solid white;" />

Peace or die!  
They blame the Lagoonies for the fact their music doesn't bring in any money.


Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Axllotus (HQ), Grunstatt, Noopolis, Ånslo |   | Guerrilla gardening | None | Dragon | It's free, man

-----------------------
 
#### Police Force Now!
<img src="../Resources/Textures/Decals/Police.png" style="background-color:royalblue; border: 23px solid silver;"/>

Spotting targets is so much easier when you're dressed in blue. And these people know it. Also recruiting new goons works better with a catchy name.  
They get money every time a bomb goes off.


Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
Anywhere but Noopolis, HQ in Airstrip 1 | Police cap, Shades | Siren in any vehicle | Never been in a gang | Snake | ϕ 10,-

-----------------------

#### Zebratools Inc.

<img src="../Resources/Textures/Decals/Zebratools.png" style="background: linear-gradient(to bottom, black, transparent, transparent, transparent, white), linear-gradient(to right, black, white, white, black, black); background-size: 100%, 21% 100%; border: 23px solid plum;" />

Power tools, pharmaceuticals and fashion. They loathe the AAA for threatening their business and try to keep them small. Startled by cops and understanding towards Pantherae Byssii.


Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
Sloompur (HQ), Monkong, Zombuntu, Noopolis | Fedora | Perma-member, perma-death, kick and LOIC | Experience | Horse | ϕ 100,-

-----------------------

#### Luciferians
<img src="../Resources/Textures/Decals/Luciferians.png" style="background: linear-gradient(to bottom, black, black, black, orange); border: 23px solid red;" />


Driven by demoney forces - aka the _almighty buck_ - and bodily pleasures under the motto "Be evil" and believing in blood magic and the power of buck dancing. They are constantly trying to prove that there is no greater evil on Art, Obscurinati included.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Ichell (HQ), Ånslo, Agrobar, Balkan | Pendant | Knife (lethal punch), Armored cars | Wealth | Goat | ϕ 666,-

##### Ranks
1| 2
---|---
Young buck | Ram rider

-----------------------

#### Untied Morons o' Murky

<img src="../Resources/Textures/Decals/UMoM.png" style="background: linear-gradient(to bottom, blue, blue, transparent, transparent, transparent), linear-gradient(to right, red, red, white, white, red); background-repeat: repeat-x; background-size: 100px 100%;"/>

A (white) Christian biker gang, all that's left of a sunken empire. Stupidity is their motto, blindness their creed and think they're better then most people because Moron starts with "more". They consider the Pantherae Byssii to be a living nightmare from their dark past.


Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Noopolis (HQ), Airstrip 1, Zombuntu | Bandanna, Shades | Bullet resistant | Fat | Monkey | ϕ 10,-

-----------------------

#### SubGenii

<img src="../Resources/Textures/Decals/SubGenii.png" style="background-color: crimson; border: 23px solid black;" />

Realizing they'll need to make some more noise if they want to reclaim the title of biggest religion, that's exactly what they'll do.


Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
 Balkan (HQ), Noopolis, Grunstatt| Pipe | Tracking and sharing data | Male | Rooster | ϕ 20,-

-----------------------

#### Lagoonies


<img src="../Resources/Textures/Decals/Lagoonies.png" style="background-color:purple; border: 23px solid orange;"/>


All these people became criminals overnight for sharing audio and decided to stop running forcing them to break many other laws as well.


Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
Ånslo (HQ), Grunstatt, Noopolis | Eyepatch, Bicorne, Tricorne, Bandanna, Visor | Tracking and sharing data |  | Dog | ϕ 5,-

-----------------------

#### Bio-Front
<img src="../Resources/Textures/Decals/BioFront.png" style="background-color: green; border: 23px solid skyblue;" />

Fighting pollution, freeing animals and slaughtering meat eaters. They despise police and are in contact with being from the Vega system.

Hometown | Acessories | Special | Criteria | Zodiac | Upkeep
---|---|---|---|---|---
Grunstatt (HQ), Noopolis, Axllotus, Balkan | | Saboteurs | Not too fat | Pig | ϕ 34,-

-----------------------

#### X-Terminators
<img src="../Resources/Textures/Decals/XTerminators.png" style="background-color: black; border: 23px solid chartreuse;" />

These black budget androids from the recent past just got an unholy update from future Martian prankster ghosts that originated from planet X. It makes these machines highly annoyed by all stuff that moves. Being unable to turn off their cameras, some have "lost" an arm. They hate the AAA to their cores.



Hometown | Acessories | Special | Criteria | Zodiac | Profit
---|---|---|---|---|---
None, admins and bots only | Shades | Run fast and flip cars | Mechanical | Cat | ϕ1000,-