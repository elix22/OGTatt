/* OG Tatt
// Copyright (C) 2016 LucKey Productions (luckeyproductions.nl)
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/

#include "player.h"
#include "bullet.h"
#include "muzzle.h"
#include "hitfx.h"
#include "ogtattcam.h"
#include "spawnmaster.h"
#include "inputmaster.h"
#include "vehicle.h"

#include "cookiejar.h"

#include "character.h"

void Character::RegisterObject(Context *context)
{
    context->RegisterFactory<Character>();
}

Character::Character(Context* context):
    Controllable(context),
    male_{static_cast<bool>(Random(2))},
    maxHealth_{100.0f},
    hairStyle_{static_cast<Hair>(Random(HAIR_ALL))},
    sinceLastTurn_{},
    turnInterval_{},
    objective_{ nullptr },
    target_{}
{
}


void Character::OnNodeSet(Node *node)
{ if (!node) return;

    Controllable::OnNodeSet(node_);

    node_->SetRotation(Quaternion(Random(360.0f), Vector3::UP));

    //Setup physics components
    rigidBody_->SetMass(8.0f);
    rigidBody_->SetFriction(0.0f);
    rigidBody_->SetRestitution(0.5f);
    rigidBody_->SetLinearDamping(0.95f);
    rigidBody_->SetAngularFactor(Vector3::UP);
    rigidBody_->SetLinearRestThreshold(0.001f);
    rigidBody_->SetAngularRestThreshold(0.01f);
    rigidBody_->SetAngularDamping(1.0f);

//    shot_sfx = MC->GetSample("Shot_m");

    //Set up graphics components
    CreateBody();

    animCtrl_ = node_->CreateComponent<AnimationController>();
    animCtrl_->PlayExclusive("Models/IdleRelax.ani", 1, true, 0.15f);
    animCtrl_->SetSpeed("Models/IdleRelax.ani", 1.0f);
    animCtrl_->SetStartBone("Models/IdleRelax.ani", "MasterBone");

    collisionShape_->SetCylinder(0.4f, 0.9f, Vector3::UP * 0.45f);

    SubscribeToEvent(node_, E_NODECOLLISIONSTART, URHO3D_HANDLER(Character, HandleNodeCollisionStart));
    SubscribeToEvent(E_SCENEDRAWABLEUPDATEFINISHED,   URHO3D_HANDLER(Character, UpdateKinematics));
}

void Character::CreateBody()
{
    for (int c{0}; c < 5; ++c) {
        switch (c){
        case 0:{
            colors_.Push(LucKey::RandomSkinColor());
        } break;
        case 4:{
            colors_.Push(LucKey::RandomHairColor());
        } break;
        default: colors_.Push(LucKey::RandomColor()); break;
        }
    }

    if (male_)
        model_->SetModel(MC->GetModel("Male"));
    else
        model_->SetModel(MC->GetModel("Female"));

    model_->SetMorphWeight(0, Random());

    for (unsigned m{0}; m < model_->GetNumGeometries(); ++m){
        model_->SetMaterial(m, CACHE->GetTempResource<Material>("Materials/Basic.xml"));
        Color diffColor{ colors_[m] };
        if (m == 4){
            if (hairStyle_ == HAIR_BALD)
                diffColor = colors_[0];
            else if (hairStyle_ == HAIR_MOHAWK)
                diffColor = LucKey::RandomHairColor(true);
        }
        model_->GetMaterial(m)->SetShaderParameter("MatDiffColor", diffColor);
        Color specColor{ diffColor * (1.0f - 0.1f * m) };
        specColor.a_ = 23.0f - 4.0f * m;
        model_->GetMaterial(m)->SetShaderParameter("MatSpecColor", specColor);
    }

    if (hairStyle_){
        Node* hairNode{ node_->GetChild("Head", true)->CreateChild("Hair") };
        hairNode->SetScale(1.0f - (0.13f * !male_));
        hairModel_ = hairNode->CreateComponent<AnimatedModel>();
        hairModel_->SetCastShadows(true);

        switch (hairStyle_){
        default: case HAIR_BALD: case HAIR_SHORT: hairModel_->SetModel(nullptr); break;
        case HAIR_MOHAWK:
            hairModel_->SetModel(MC->GetModel("Mohawk"));
            break;
        case HAIR_SEAGULL:
            hairModel_->SetModel(MC->GetModel("Seagull"));
                    break;
        case HAIR_MUSTAIN:
            hairModel_->SetModel(MC->GetModel("Mustain"));
                    break;
        case HAIR_FROTOAD:
            hairModel_->SetModel(MC->GetModel("Frotoad"));
                    break;
        case HAIR_FLATTOP:
            hairModel_->SetModel(MC->GetModel("Flattop"));
            break;
        }
        if (hairStyle_ != HAIR_BALD && hairStyle_ != HAIR_SHORT)
        {
            hairModel_->SetMorphWeight(0, Random());

            //Set color for hair model
            hairModel_->SetMaterial(CACHE->GetTempResource<Material>("Materials/Basic.xml"));
            Color diffColor = colors_[4];
            hairModel_->GetMaterial()->SetShaderParameter("MatDiffColor", diffColor);
            Color specColor{ diffColor * 0.23f };
            specColor.a_ = 5.0f;
            hairModel_->GetMaterial()->SetShaderParameter("MatSpecColor", specColor);
        }
    }

//    for (Node* node: node_->GetChildren(true))
//        Log::Write(LOG_INFO, node->GetName());
}

void Character::Set(Vector3 position)
{
    Controllable::Set(position);
    health_= maxHealth_;
}

void Character::Update(float timeStep)
{ if (!node_->IsEnabled())
        return;

    if (!IsAlive()) {

        PODVector<RigidBody*> limbs{};
        node_->GetComponents<RigidBody>(limbs, true);
        bool activeBody{ false };

        for (RigidBody* r : limbs) {

            if (r->IsActive() && r->GetGravityOverride().Length() < 42.0f) {

                r->SetGravityOverride(r->GetGravityOverride() * (1.0f + timeStep));
                activeBody |= true;
            }
        }
        if (!activeBody) {

            for (RigidBody* r : limbs) {

                r->ReleaseBody();
            }

            node_->Translate(Vector3::DOWN * timeStep * 0.017f, TS_WORLD);

            if (node_->GetWorldPosition().y_ < -2.0f)
                Disable();
        }

        return;
    }

    Controllable::Update(timeStep);

    //Orientation vectors
    Player* player{ GetSubsystem<InputMaster>()->GetPlayerByControllable(this) };
    if (player){
        OGTattCam* cam{ MC->GetCamera(player->GetPlayerId()) };
        if (cam){
            Vector3 camRight{ cam->GetNode()->GetRight() };
            Vector3 camForward{ cam->GetNode()->GetDirection() };
            camRight = (camRight * (Vector3::ONE - Vector3::UP)).Normalized();
            camForward = (camForward * (Vector3::ONE - Vector3::UP)).Normalized();
        }
    }

    //Update rotation according to direction of the player's movement.
    Vector3 velocity{ rigidBody_->GetLinearVelocity() * Vector3(1.0f, 0.0f, 1.0f) };
    Vector3 lookDirection{ velocity + 2.0f * aim_ * .3f };
    Quaternion rotation{ node_->GetWorldRotation() };
    Quaternion aimRotation{ rotation };
    aimRotation.FromLookRotation(lookDirection);
    node_->SetRotation(rotation.Slerp(aimRotation, 7.0f * timeStep * velocity.Length()));

    //Update animation
    if (velocity.Length() > 0.1f){

        animCtrl_->PlayExclusive("Models/WalkRelax.ani", 1, true, 0.15f);
        animCtrl_->SetSpeed("Models/WalkRelax.ani", velocity.Length() * (2.3f - actions_[RUN] * 0.55f));
        animCtrl_->SetStartBone("Models/WalkRelax.ani", "MasterBone");
    }
    else if (health_ < maxHealth_){

        animCtrl_->StopAll(0.23f);
        animCtrl_->PlayExclusive("Models/IdleAlert.ani", 0, true, 0.15f);
        animCtrl_->SetStartBone("Models/IdleAlert.ani", "MasterBone");
    } else {
        animCtrl_->StopAll(0.23f);
        animCtrl_->PlayExclusive("Models/IdleRelax.ani", 0, true, 0.1f);
        animCtrl_->SetStartBone("Models/IdleRelax.ani", "MasterBone");
    }

    //Shooting
    if (shotInterval_ == 0.0f)
        return;

    float bulletHeight{ 0.55f };

    sinceLastShot_ += timeStep;

    if (aim_.Length() > 0.1f) {
        if (sinceLastShot_ > shotInterval_)
        {
            sinceLastShot_ = 0.0;
            Bullet* bullet{ SPAWN->Create<Bullet>() };
            bullet->Set(node_->GetPosition() + Vector3::UP * bulletHeight + collisionShape_->GetSize().x_ * 0.5f * aim_, aim_, node_);

            Muzzle* muzzle{ SPAWN->Create<Muzzle>() };
            muzzle->Set(node_->GetPosition() + Vector3::UP * bulletHeight + 0.1f * aim_, aim_);

            PlaySample(MC->GetSample("Shot_m"));
        }
    }
}
void Character::FixedUpdate(float timeStep)
{
    if (IsAlive()) {

        //Movement values
        float thrust{ 2342.0f };
        float maxSpeed{ 0.5f + 1.5f * health_ / maxHealth_ };

        //Apply movement
        Vector3 force{ move_ * thrust * (1.0f + 1.666f * actions_[RUN]) * timeStep };
        rigidBody_->ApplyForce(force * Max(0.0f, 1.0f - Clamp(rigidBody_->GetLinearVelocity().DotProduct(move_) - maxSpeed, 0.0f, 2.0f)));
    }
}
void Character::HandleAction(int actionId)
{
    switch (actionId) {
    case ENTER:
        EnterNearestVehicle();
        break;
    case JUMP:
        shotInterval_ = 0.1f;
        break;
    default:
        break;
    }
}

void Character::HandleNodeCollisionStart(StringHash eventType, VariantMap& eventData)
{
    MemoryBuffer contacts{ eventData[NodeCollisionStart::P_CONTACTS].GetBuffer() };

    while (!contacts.IsEof())
    {
        Vector3 contactPosition{ contacts.ReadVector3() };
        Vector3 contactNormal{ contacts.ReadVector3() };
        float contactDistance{ contacts.ReadFloat() };
        float contactImpulse{ (1.0f - contactNormal.y_ * 0.5f) * contacts.ReadFloat() };

        if (contactImpulse > 13.0f){

            Node* otherNode{ static_cast<Node*>(eventData[NodeCollisionStart::P_OTHERNODE].GetPtr()) };
            RigidBody* otherBody{ static_cast<RigidBody*>(eventData[NodeCollisionStart::P_OTHERBODY].GetPtr()) };

            float damage{ Sqrt(contactImpulse) * (2.0f + Sqrt(otherBody->GetMass() * otherBody->GetLinearVelocity().Length())) };
            if (damage < 5.0f)
                return;

            Hit(damage);
            HitFX* splat{ SPAWN->Create<HitFX>() };
            splat->Set(contactPosition, Substance::Flesh);
            splat->GetNode()->SetScale(Clamp(Sqrt(damage) * 0.034f, 1.0f, 10.0f));

            if (!otherNode->GetParent()->HasComponent<Level>())
                SetObjective(otherNode);
        }
    }
}

void Character::UpdateKinematics(StringHash eventType, VariantMap& eventData)
{
    bool armed{ shotInterval_ != 0.0f };

    if (IsAlive() && aim_.Length()) {

        if (Node* chest = node_->GetChild("Chest", true)) {

            Vector3 cross{ 1.17f * M_RADTODEG * aim_.CrossProduct(chest->GetWorldDirection().ProjectOntoPlane(Vector3::ZERO, Vector3::UP)) };
            chest->Rotate(Quaternion(cross.y_ - 23.0f, Vector3::UP), TS_WORLD);
        }
        if (Node* neck = node_->GetChild("Neck", true)) {

            Vector3 cross{ M_RADTODEG * aim_.CrossProduct(neck->GetWorldDirection()) };
            neck ->Rotate(Quaternion(cross.y_, Vector3::UP), TS_WORLD);
        }
        if (Node* head = node_->GetChild("Head", true)) {

            Vector3 cross{ M_RADTODEG * aim_.CrossProduct(head->GetWorldDirection()) };
            head->Rotate(Quaternion(cross.y_, Vector3::UP), TS_WORLD);
        }
        if (armed) {

            if (Node* shoulder = node_->GetChild("Shoulder.R", true)) {

                Vector3 cross{ M_RADTODEG * aim_.CrossProduct(shoulder->GetWorldDirection()) };
                shoulder ->Rotate(Quaternion(cross.y_, Vector3::UP), TS_WORLD);
            }
            if (Node* arm = node_->GetChild("UpperArm.R", true)) {

                target_ = arm->GetWorldPosition() + Quaternion(90.0f, Vector3::UP) * aim_.Normalized() * 2.3f;
                arm->LookAt(target_, aim_ + Vector3::UP);
            }
        }
    }
}
void Character::Hit(float damage)
{
    health_ -= damage;

    if (IsAlive() && health_ <= 0.0f) {

        Die();
    }
}
void Character::Die()
{
    InputMaster* inputMaster{ GetSubsystem<InputMaster>() };
    if (inputMaster->GetControllables().Contains(this))
        inputMaster->SetPlayerControl(GetPlayer(), nullptr);

    node_->AddTag("Dead");
    CreateRagdoll();
}
void Character::CreateRagdoll()
{
    animCtrl_->SetEnabled(false);

    Vector3 velocity{ rigidBody_->GetLinearVelocity() };

    rigidBody_->ReleaseBody();
    node_->RemoveComponent<RigidBody>();
    node_->RemoveComponent<CollisionShape>();
    node_->RemoveComponent<AnimationController>();

    CreateRagdollBone("Hips",  SHAPE_BOX,     Vector3(0.3f, 0.23f, 0.1f),  Vector3(0.0f, 0.05f, -0.05f), Quaternion::IDENTITY);
    CreateRagdollBone("Chest", SHAPE_BOX,     Vector3(0.23f, 0.3f, 0.1f),  Vector3(0.0f, 0.0f,   0.0f),  Quaternion::IDENTITY);
    CreateRagdollBone("Neck",  SHAPE_CAPSULE, Vector3(0.08f, 0.1f, 0.08f), Vector3(0.0f, 0.0f,   0.0f),  Quaternion::IDENTITY);
    CreateRagdollBone("Head",  SHAPE_CAPSULE, Vector3(0.13f, 0.2f, 0.13f), Vector3(0.0f, 0.05f,  0.0f),  Quaternion::IDENTITY);

    CreateRagdollConstraint("Head", "Neck",  CONSTRAINT_CONETWIST, Vector3::UP, Vector3::UP, Vector2(45.0f, 45.0f), Vector2(-20.0f, -20.0f), true);
    CreateRagdollConstraint("Neck", "Chest", CONSTRAINT_HINGE, Vector3::LEFT, Vector3::LEFT, Vector2(45.0f, 45.0f), Vector2(-20.0f, -20.0f), true);
    CreateRagdollConstraint("Chest", "Hips", CONSTRAINT_CONETWIST, Vector3::UP, Vector3::UP, Vector2(45.0f, 45.0f), Vector2(-20.0f, -20.0f), true);

    CreateRagdollBone("UpperArm.L", SHAPE_CAPSULE, Vector3(0.11f, 0.23f, 0.11f), Vector3(0.0f, 0.07f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("UpperArm.R", SHAPE_CAPSULE, Vector3(0.11f, 0.23f, 0.11f), Vector3(0.0f, 0.07f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("LowerArm.L", SHAPE_CAPSULE, Vector3(0.1f, 0.2f, 0.1f), Vector3(0.0f, 0.1f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("LowerArm.R", SHAPE_CAPSULE, Vector3(0.1f, 0.2f, 0.1f), Vector3(0.0f, 0.1f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Hand.L",     SHAPE_BOX,     Vector3(0.1f, 0.1f, 0.07f), Vector3(0.0f, 0.05f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Hand.R",     SHAPE_BOX,     Vector3(0.1f, 0.1f, 0.07f), Vector3(0.0f, 0.05f, 0.0f), Quaternion::IDENTITY);

    CreateRagdollConstraint("UpperArm.L", "Chest", CONSTRAINT_CONETWIST, Vector3::UP, Vector3::RIGHT, Vector2(60.0f, 90.0f), Vector2(-45.0f, -45.0f), true);
    CreateRagdollConstraint("UpperArm.R", "Chest", CONSTRAINT_CONETWIST, Vector3::UP, Vector3::LEFT, Vector2(60.0f, 90.0f), Vector2(-45.0f, -45.0f), true);
    CreateRagdollConstraint("LowerArm.L", "UpperArm.L", CONSTRAINT_HINGE, Vector3::FORWARD, Vector3::FORWARD, Vector2(120.0f, 120.0f), Vector2(0.0f, 0.0f), true);
    CreateRagdollConstraint("LowerArm.R", "UpperArm.R", CONSTRAINT_HINGE, Vector3::FORWARD, Vector3::FORWARD, Vector2(120.0f, 120.0f), Vector2(0.0f, 0.0f), true);
    CreateRagdollConstraint("Hand.L",     "LowerArm.L", CONSTRAINT_HINGE, Vector3::RIGHT, Vector3::RIGHT, Vector2(90.0f, 90.0f), Vector2(-45.0f, -45.0f), true);
    CreateRagdollConstraint("Hand.R",     "LowerArm.R", CONSTRAINT_HINGE, Vector3::LEFT, Vector3::LEFT, Vector2(90.0f, 90.0f), Vector2(-45.0f, -45.0f), true);

    CreateRagdollBone("Thigh.L",  SHAPE_CAPSULE, Vector3(0.13f, 0.23f, 0.13f), Vector3(0.0f, 0.07f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Thigh.R",  SHAPE_CAPSULE, Vector3(0.13f, 0.23f, 0.13f), Vector3(0.0f, 0.07f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Shin.L",   SHAPE_CAPSULE, Vector3(0.1f, 0.23f, 0.1f), Vector3(0.0f, 0.1f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Shin.R",   SHAPE_CAPSULE, Vector3(0.1f, 0.23f, 0.1f), Vector3(0.0f, 0.1f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Instep.L", SHAPE_BOX,     Vector3(0.1f, 0.1f, 0.07f), Vector3(0.0f, 0.05f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Instep.R", SHAPE_BOX,     Vector3(0.1f, 0.1f, 0.07f), Vector3(0.0f, 0.05f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Toes.L",   SHAPE_BOX,     Vector3(0.1f, 0.1f, 0.07f), Vector3(0.0f, 0.05f, 0.0f), Quaternion::IDENTITY);
    CreateRagdollBone("Toes.R",   SHAPE_BOX,     Vector3(0.1f, 0.1f, 0.07f), Vector3(0.0f, 0.05f, 0.0f), Quaternion::IDENTITY);

    CreateRagdollConstraint("Thigh.L", "Hips",   CONSTRAINT_CONETWIST, Vector3::LEFT,      Vector3::RIGHT,   Vector2( 45.0f,  10.0f),   Vector2(-45.0f, -10.0f), true);
    CreateRagdollConstraint("Thigh.R", "Hips",   CONSTRAINT_CONETWIST, Vector3::RIGHT,      Vector3::LEFT,    Vector2( 45.0f,  10.0f),   Vector2(-45.0f, -10.0f), true);
    CreateRagdollConstraint("Shin.L", "Thigh.L",  CONSTRAINT_HINGE,     Vector3::LEFT, Vector3::LEFT, Vector2(120.0f, 120.0f),   Vector2(  0.0f,   0.0f), true);
    CreateRagdollConstraint("Shin.R", "Thigh.R",  CONSTRAINT_HINGE,     Vector3::RIGHT, Vector3::RIGHT, Vector2(120.0f, 120.0f),   Vector2(  0.0f,   0.0f), true);
    CreateRagdollConstraint("Instep.L", "Shin.L", CONSTRAINT_HINGE,     Vector3::RIGHT,   Vector3::RIGHT,   Vector2( 45.0f,  45.0f),   Vector2(-23.0f, -23.0f), true);
    CreateRagdollConstraint("Instep.R", "Shin.R", CONSTRAINT_HINGE,     Vector3::LEFT,    Vector3::LEFT,    Vector2( 45.0f,  45.0f),   Vector2(-23.0f, -23.0f), true);
    CreateRagdollConstraint("Toes.L", "Instep.L", CONSTRAINT_HINGE,     Vector3::RIGHT,   Vector3::RIGHT,   Vector2( 10.0f,  10.0f),   Vector2(-10.0f, -10.0f), true);
    CreateRagdollConstraint("Toes.R", "Instep.R", CONSTRAINT_HINGE,     Vector3::LEFT,    Vector3::LEFT,    Vector2( 10.0f,  10.0f),   Vector2(-10.0f, -10.0f), true);

    PODVector<RigidBody*> limbs{};
    node_->GetComponents<RigidBody>(limbs, true);
    for (RigidBody* r : limbs) {

        r->SetLinearVelocity(velocity);
        r->ResetForces();
    }
}

void Character::CreateRagdollBone(const String& boneName, ShapeType type, const Vector3& size, const Vector3& position,
    const Quaternion& rotation)
{
    // Find the correct child scene node recursively
    Node* boneNode{ node_->GetChild(boneName, true) };
    if (!boneNode)
        return;

    RigidBody* body{ boneNode->CreateComponent<RigidBody>() };
    body->SetMass(100.0f * size.x_ * size.y_ * size.z_);
    body->SetLinearDamping(0.5f);
    body->SetAngularDamping(0.8f);
    body->SetLinearRestThreshold(2.3f);
    body->SetAngularRestThreshold(5.0f);
    body->SetFriction(1.1f);
    body->SetGravityOverride(Vector3::DOWN * 9.81f);

    CollisionShape* shape{ boneNode->CreateComponent<CollisionShape>() };
    // We use either a box or a capsule shape for all of the bones
    if (type == SHAPE_BOX)
        shape->SetBox(size, position, rotation);
    else
        shape->SetCapsule(size.x_, size.y_, position, rotation);
}

void Character::CreateRagdollConstraint(const String& boneName, const String& parentName, ConstraintType type,
    const Vector3& axis, const Vector3& parentAxis, const Vector2& highLimit, const Vector2& lowLimit,
    bool disableCollision)
{
    Node* boneNode{ node_->GetChild(boneName, true) };
    Node* parentNode{ node_->GetChild(parentName, true) };
    if (!boneNode || !parentNode)
        return;

    Constraint* constraint{ boneNode->CreateComponent<Constraint>() };
    constraint->SetConstraintType(type);
    constraint->SetDisableCollision(disableCollision);
    constraint->SetOtherBody(parentNode->GetComponent<RigidBody>());
    constraint->SetWorldPosition(boneNode->GetWorldPosition());
    constraint->SetAxis(axis);
    constraint->SetOtherAxis(parentAxis);
    constraint->SetHighLimit(highLimit);
    constraint->SetLowLimit(lowLimit);
}

Substance Character::GetSubstance()
{
    return Substance::Flesh;
}

void Character::Think(float timeStep)
{
    actions_[RUN] = health_ != maxHealth_;

    if (objective_ != nullptr) {

        if (node_->GetWorldPosition().DistanceToPoint(objective_->GetWorldPosition()) < 5.0f)
            aim_ = (objective_->GetWorldPosition() - node_->GetWorldPosition()).Normalized();
        else
            aim_ = Vector3::ZERO;

        if (Character* character = objective_->GetComponent<Character>()) {

            if (!character->IsAlive())
                SetObjective(nullptr);

        } else if (Vehicle* vehicle = objective_->GetDerivedComponent<Vehicle>()) {

            if (!vehicle->IsFunctional())
                SetObjective(nullptr);
        }

    }

    sinceLastTurn_ += timeStep;

    if (sinceLastTurn_ > turnInterval_){
        sinceLastTurn_ = 0.0f;
        turnInterval_ = Random(2.3f, 5.0f);
        int r = Random(5 - actions_[RUN]);

        switch (r) {
        case 0:
            move_ = Vector3::FORWARD;
            break;
        case 1:
            move_ = Vector3::LEFT;
            break;
        case 2:
            move_ = Vector3::BACK;
            break;
        case 3:
            move_ = Vector3::RIGHT;
            break;
        case 4:
            move_ = Vector3::ZERO;
            break;
        default:
            break;
        }

        if (r != 4) {
            move_ += Vector3(Random(-0.1f, 0.1f), 0.0f, Random(-0.1f, 0.1f));
        }

        move_ *= 0.5f + 0.5f * actions_[RUN];
    }

    if (move_.DotProduct(node_->GetWorldPosition().Normalized()) > 0.25f)
        turnInterval_ *= 1.0f - timeStep;
}

void Character::EnterNearestVehicle()
{
     EnterVehicle(MC->GetNearestInstanceOf<Vehicle>(GetWorldPosition()));
}
void Character::EnterVehicle(Vehicle* vehicle)
{
    if (vehicle && vehicle->IsFunctional())
        if (Player* player = GetPlayer()) {

            GetSubsystem<InputMaster>()->SetPlayerControl(player, vehicle);
            vehicle->SetLightsEnabled(true);
        }
}
